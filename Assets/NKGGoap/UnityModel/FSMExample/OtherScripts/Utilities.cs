﻿using System.Collections.Generic;
using UnityEngine;

namespace ReGoap.Unity.FSMExample.OtherScripts
{
    /// <summary>
    /// 实用函数集
    /// </summary>
    public static class Utilities
    {
        /// <summary>
        /// 获取最近地点
        /// </summary>
        /// <param name="thisPosition"></param>
        /// <param name="otherPositions"></param>
        /// <param name="stopOnDistance"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetNearest<T>(Vector3 thisPosition, Dictionary<T, Vector3> otherPositions, float stopOnDistance = 1f) where T : class
        {
            T best = null;
            var bestDistance = float.MaxValue;
            foreach (var pair in otherPositions)
            {
                var otherPosition = pair.Value;
                var delta = (thisPosition - otherPosition).sqrMagnitude;
                if (delta < bestDistance)
                {
                    bestDistance = delta;
                    best = pair.Key;
                }
                if (delta <= stopOnDistance)
                    break;
            }
            return best;
        }
    }
}